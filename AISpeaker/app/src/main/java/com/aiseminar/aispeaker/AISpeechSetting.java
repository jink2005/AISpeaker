package com.aiseminar.aispeaker;

import com.aiseminar.aispeaker.aimouth.AIMouth;
import android.app.Activity;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.Window;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

public class AISpeechSetting extends Activity {
	public static final String SP_SPEECH_SETTING = "SP_SPEECH_SETTING";
	public static final String SP_SPEED_KEY = "SP_SPEED_KEY";
	public static final int SPEECH_SPEED_DEFAULT = 60;
	
	private SharedPreferences mSPSpeechSetting = null;
	private int mSpeed;
	private SeekBar mSpeedSeekBar = null;
	private TextView mTvSpeedValue = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE); 
		setContentView(R.layout.layout_speech_setting);
		
		mSPSpeechSetting = getSharedPreferences(SP_SPEECH_SETTING, MODE_PRIVATE);
		mSpeed = mSPSpeechSetting.getInt(SP_SPEED_KEY, SPEECH_SPEED_DEFAULT);
		
		mSpeedSeekBar = (SeekBar) findViewById(R.id.sbSpeechSpeed);
		mSpeedSeekBar.setProgress(mSpeed);
		mSpeedSeekBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				AIMouth myMouth = AIMouth.getMouth(AISpeechSetting.this);
				myMouth.setSpeechSpeed(mSpeed);
				myMouth.speak(AISpeechSetting.this.getString(R.string.st_welcome));
			}
			
			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				mSpeed = progress;
				mTvSpeedValue.setText(String.valueOf(mSpeed));
			}
		});
		
		mTvSpeedValue = (TextView) findViewById(R.id.tvSpeedValue);
		mTvSpeedValue.setText(String.valueOf(mSpeed));
	}

	@Override
	protected void onDestroy() {
		Editor editor = mSPSpeechSetting.edit();
		editor.putInt(SP_SPEED_KEY, mSpeed);
		editor.commit();
		
		super.onDestroy();
	}
	
}
